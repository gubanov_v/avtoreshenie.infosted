<!DOCTYPE html>
<html lang="ru">

<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <? if ($this->_description):?>
            <meta content="<?=$this->_description?>" name="description">
        <? endif; ?>
        
        <? if ($this->_keywords):?>
            <meta content="<?=$this->_keywords?>" name="keywords">
        <? endif; ?>
        
		<title><?=$this->_title;?></title>
        <?
        $this->addCssFile("style.css");;
        ?>
        <link rel="icon" type="image/ico" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico"/>
        <link href='https://fonts.googleapis.com/css?family=Exo+2:400,800,700,900,300,100,200,500,600&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <?
        if ( $css = $this->getCss() )
        {?>
            <link href="<?=$css?>" rel="stylesheet" type="text/css"/>
        <?}

        if ( $USER->GetID() == 1 )
        {
            $ADMIN = true;
        }
        else
        {
            $ADMIN = false;
        }
        
        if ( $ADMIN )
        {
            $APPLICATION->ShowCSS();
            $APPLICATION->ShowHeadStrings();
            $APPLICATION->ShowHeadScripts();
        }

        $this->addJsFile("jquery-2.2.4.min.js");
        $this->addJsFile("ion-rangeSlider/ion.rangeSlider.js");
        $this->addJsFile("owl.carousel.min.js");
	   $this->addJsFile("main.js");

        if ( $js = $this->getJs() )
        {
            $js = explode( ",", $js);

            foreach ( $js as $j )
            {?>
                <script src="<?=$j?>" type="text/javascript"></script>
            <?}
        }?>
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".bigselect").select2();

            });
        </script>
	</head>
	<body>
    <?if ($ADMIN) $APPLICATION->ShowPanel(); ?>
    <div class="top-block">
        <div class="fixblock">
            <?/*div class="entrance-block">
                <a href="">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="16px" height="17px" viewBox="0 0 16 16">
                        <path fill="#000000" d="M7 1v2l1 1v-2h7v12h-7v-2l-1 1v2h9v-14z"></path>
                        <path fill="#000000" d="M10 8l-5-4v2h-5v4h5v2z"></path>
                    </svg> Вход</a>
                <span>|</span>
                <a href="">Зарегистрироваться</a>
            </div*/?>
            <div class="clear"></div>
        </div>
    </div>

    <div class="index hello-block">
        <div class="fixblock">
            <header>
                <a href="<?=( $_SERVER["REQUEST_URI"] == "/" ? "javascript: void(0)" : "/")?>" class="logo"></a>
                <ul class="menu">
                    <li><a href="/about/">О проекте</a></li>
                    <li><a href="/contacts/">Контакты</a></li>
                    <li><a href="/partners/">Условия партнерства</a></li>
                </ul>
            </header>
        </div>
    </div>

    <main>
