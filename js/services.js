/**
 * Created by Ilnara on 01.07.2016.
 */
var img = null;

$(document).ready(function() {

    $.ajax({
        url: path_post,
        method: "POST",
        data: { "op" : "img-mark"},
        dataType: "JSON",
        success: function(data) {
            img = data;
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

    $("#top-marks.select-big").select2({
        templateResult: formatState,
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите марку",
        width: "200px"
    });
    $("select[name=service].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите услугу",
        width: "300px"
    });
    $("select[name=district].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите район/округ",
        width: "260px"
    });
});

function formatState (state) {
    if (!state.id) { return state.text; }
    var $state = $('<span>' + img[state.element.value] + state.text + '</span>');
    return $state;
}