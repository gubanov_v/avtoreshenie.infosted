$(function() {

     /*ymaps.ready(init);
    var myMap;
    
    function init()
    {     
        myMap = new ymaps.Map("map", {
                center: [55.751574, 37.573856],
                zoom: 10,
                controls : ['zoomControl'],
        });
        
        myMap.behaviors.disable('scrollZoom');
            
        $(".park").each(function(){
            x = +$(this).data('x');
            y = +$(this).data('y');
            
            var myPlacemark = new ymaps.Placemark([x, y], {
                balloonContentBody: '<a href="/' + $(this).data('company-code') + '/" class="baloon-href">' + $(this).data('company') + '</a><p class="baloon-text">' + 
                            $(this).data('car') + '</p>',
                  },  {
                        iconLayout: 'default#image',
                        iconImageHref: '/bitrix/templates/avtoreshenie/images/map-marker.png',
                        iconImageSize: [20, 27],
                        iconImageOffset: [-10, -27]

                  });   
                
            myMap.geoObjects.add(myPlacemark);
        });
    }*/
    
    var img = null;
    
    $.ajax({
        url: path_post,
        method: "POST",
        dataType: "JSON",
        success: function(data) {
            img = data;
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

    $("#top-marks.select-big").select2({
        templateResult: formatState,
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите марку"
    });
    $("select[name=service].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите услугу"
    });
    $("select[name=district].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите район/округ"
    });

    $("#bottom-marks.select-big").select2({
        templateResult: formatState,
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите марку"
    });
     
    function formatState (state) {
    if (!state.id) { return state.text; }
    var $state = $('<span>' + img[state.element.value] + state.text + '</span>');
    return $state;
    };
 });