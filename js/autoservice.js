/**
 * Created by Ilnara on 01.07.2016.
 */
$(document).ready(function() {

    var url = document.location.href;
    if ( /#[a-z\-]+/i.test(url) )
    {
        var id = url.match(/#[a-z\-]+/i);
        $("td" + id).parent().addClass("active-service");
    }

    $(".filter-model").select2({});
    $(".filter-service").select2({
        multiple: false,
        maximumSelectionLength: 3
    });
    $(".filter-engine").select2({});

    $(".bigselect").select2();
    $(".sort-block").select2({
        minimumResultsForSearch: Infinity,
        placeholder: "Сортировка"
    });

    $("input[name=phone]").mask("+9 999 999 99 99");

    $("a#check-in").click(function(){

            var phone = $("input[name=phone]").val();

            var as_phone = $("input[name=as_phone]").val();
            var as = $("input[name=as]").val();
            var service = $("input[name=service]").val();
            var mark = $("input[name=mark]").val();


            if ( !loading )
            {
                loading = true;

                $.ajax({
                    url: "/include/libs/mailer.php",
                    method: "POST",
                    dataType: "JSON",
                    data: { "phone" : phone, "as" : as, "as_phone" : as_phone, "service" : service, "mark" : mark },
                    success: function(data) {
                        if ( data["status"] == true )
                        {
                            $('.input').html(data["msg"]);
                        }
                        else
                        {
                            $('#status-check-in').html(data["msg"]);
                        }
                    },
                    complete: function() {
                        loading = false;
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        //alert(textStatus);
                        $('#status-check-in').html("Заявку не удалось отправить. Попробуйте позднее.");
                    }
                });
            }

            return false;
        });
});





/*

$(window).one("scroll", function() {
    float_top = $(".top-header").offset().top;
});


$(window).scroll(function() {
    var top = $(this).scrollTop();
    console.log(top, float_top);


    if (top > float_top) {
        $(".top-header").addClass("sticky");
//
        $(".slogan + .review-card").addClass("sticky-margin");
    } else {
        $(".top-header").removeClass("sticky");
        $(".slogan + .review-card").removeClass("sticky-margin");
    }



});*/