/**
 * Created by Ilnara on 01.07.2016.
 */
var img = null;

$(document).ready(function() {

    $.ajax({
        url: path_post,
        method: "POST",
        data: { "op" : "img-mark"},
        dataType: "JSON",
        success: function(data) {
            img = data;
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

    $("#top-marks.select-big").select2({
        templateResult: formatState,
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите марку",
        width: "200px"
    });
    $("select[name=service].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите услугу",
        width: "300px"
    });
    $("select[name=district].select-big").select2({
        theme: "classic",
        maximumSelectionLength: 1,
        placeholder: "Выберите район/округ",
        width: "260px"
    });

    $(".sort-block").select2({
        minimumResultsForSearch: Infinity,
        placeholder: "Сортировка"
    });

  //  getListAutoservices();

    $("select[name=sortby]").change(function(){
        var sortBy = this.value;
       // location.href = path_post + "?sortby=" + sortBy;
        getListAutoservices(sortBy);
    });

    $("a.list-services").click(function(){
        var as = this.id;
        var href = $(this).parents("div.company-card__price").siblings("div.company-card__info").find("div.info__header a:first-child").attr("href");
        $("div#list-services p").html("");

        $.ajax({
            url: path_post,
            method: "POST",
            dataType: "JSON",
            data: {"op" : "list-services", "as" : as },
            success: function(data) {

                if ( data.status )
                {
                    var data_obj = data.data;
                    var li = "";

                    for ( var i in data_obj )
                    {
                        li += "<li><a href=" + href + "#" + data_obj[i].code + ">" + data_obj[i].name + "</a></li>";
                    }

                    $("div#list-services p").append("<ul>" + li + "</ul>");
                }
                else
                {
                    $("div#list-services p").html(data.msg);
                }
            },
            complete: function() {
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    });
});

function formatState (state) {
    if (!state.id) { return state.text; }
    var $state = $('<span>' + img[state.element.value] + state.text + '</span>');
    return $state;
}

function getListAutoservices(sortBy)
{
    $.ajax({
        url: path_post,
        method: "POST",
        data: {"op" : "sortBy", "sortby" : sortBy },
        beforeSend: function() {
            $("#sort").html("Автосервисы загружаются");
        },
        success: function(data) {
            $("#sort").html(data);
        },
        complete: function() {
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}