path_post = window.location.pathname;
loading = false;

function changeFilterAction(mark, service, district, par)
{
	if ( service )
	{
		act = '/' + service + '/';

		if ( mark )
		{
			act = "/" + service + '-' + mark + '/';

			if ( district && par )
			{
				if ( par == "Округа" )
				{
					act = "/" + service + '-' + mark + '-v-' + district + '/';
				}
				else
				{
					act = "/" + service + '-' + mark + '-rayon-' + district + '/';
				}
			}
		}
		else
		{
			if ( district && par )
			{
				if ( par == "Округа" )
				{
					act = "/" + service + '-v-' + district + '/';
				}
				else
				{
					act = "/" + service + '-rayon-' + district + '/';
				}
			}
		}
	}
	else
	{
		if ( mark )
		{
			act = "/avtoservisy-" + mark + '/';

			if ( district && par )
			{
				if ( par == "Округа" )
				{
					act = "/remont-" + mark + '-v-' + district + '/';
				}
				else
				{
					act = "/remont-" + mark + '-rayon-' + district + '/';
				}
			}
		}
		else
		{
			if ( district && par )
			{
				if ( par == "Округа" )
				{
					act = "/avtoservisy-v-" + district + '/';
				}
				else
				{
					act = "/avtoservisy-rayon-" + district + '/';
				}
			}
			else
			{
				act = "/avtoservisy/";
			}
		}
	}

	location.href = act;

}

function getCount()
{
	var service = $("select[name=service]").val();
	var mark = $("select[name=mark-model]").val();
	var district = $("select[name=district]").val();
	var par = $("select[name=district] option:selected").parent().attr("label");

	$.ajax({
		url: path_post,
		method: "POST",
		dataType: "JSON",
		data: {"op" : "count-as", "service" : service, "mark" : mark, "district" : district, "par" : par },
		success: function(data) {
			if ( data.status )
			{
				$("input[name=submit].orange").html(data.count);
			}
			else 
			{
				$("input[name=submit].orange").html(data.msg);
			}
		},
		complete: function() {
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
}


$(document).ready(function() {
    
    // Тщетная попытка сделать скролл
    $('.select2-results__options').jScrollPane({});
    $('.test').jScrollPane({});
    
    // Анимация первого экрана
    $('.choose-block .submit').click(function(){
      $('.hello-block').addClass('city-animate');
       $('.hello-block__car').addClass('car-animate'); 
       $('.hello-block__service').addClass('service-animate'); 
    });


	$("a").click(function(){
		if ( $(this).attr("href") == window.location.pathname || $(this).attr("href") == "" )
		{
			return false;
		}
	});

	$("input.submit.button.orange.bigersize").click(function()
	{
		var service = $("select[name=service]").val();
		var mark = $("select[name=mark-model]").val();
		var district = $("select[name=district]").val();
		var par = $("select[name=district] option:selected").parent().attr("label");

		changeFilterAction(mark, service, district, par);
	});

	$("#filter-problem div.more").click(function(){
		$("#filter-problem div.filter-more").slideToggle("slow");
		$("#filter-problem div.more").toggleClass("act");
		return false;
	});
	$("#filter-brand div.more").click(function(){
		$("#filter-brand div.filter-more").slideToggle("slow");
		$("#filter-brand div.more").toggleClass("act");
		return false;
	});
//	$("div.filtr3 div.more").click(function(){
//		$("div.filtr3 div.filter-more").slideToggle("slow");
//		$("div.filtr3 div.more").toggleClass("act");
//		return false;
//	});
    
    
    /* topfilter */
    $(".show-top-filter").click(function(){
        
		$(".filtertop").slideToggle(300);
        
        
//		$(".year").slideToggle("slow");
//		$(".service").slideToggle("slow");
//		$(".engine-type").slideToggle("slow");
//		$(".sort").slideToggle("slow");
        
        if ($(this).data("mode") != 1) {
                    $(this).text('закрыть');
            $(".show-top-filter").animate({"margin-top" : "140px"},300);
            $(".show-top-wrapper").css("box-shadow", "none");
         
                    $(this).data("mode", 1);
                } else {
                       $(".show-top-filter").animate({"margin-top" : "0px"},300);
                    $(".show-top-wrapper").css("box-shadow", "0 5px 5px rgba(0, 0, 0, 0.15)");
                    $(this).text('открыть фильтр');
                    $(this).data("mode", 0);
                }
		return false;
	});
	

	/* griid */

	$(".tabs .button-view--row").click(function(){
		$(".button-view--row").addClass("active");
		$(".button-view--tile").removeClass("active");
//		$("div.service-card").removeClass("tile"); 
		$("div.service-card").addClass("row");
		return false;
	});
	$(".tabs .button-view--tile").click(function(){
		$(".button-view--tile").addClass("active");
		$(".button-view--row").removeClass("active");
		$("div.service-card").removeClass("row");
//		$("div.service-card").addClass("tile");
		return false;
	});
	/* griid */

	/* меню аккордион */

	var accordion_head = $('nav.menu[data-rolle=hirzontal] ul li.haveChildren'),
            accordion_body = $('nav.menu[data-rolle=hirzontal] ul li.haveChildren a > ul.sbmenu');
 
        // Open the first tab on load
 
// accordion_head.first().addClass('act').next().slideDown('normal');
 
        // Click function
 
        accordion_head.on('click', function(event) {
 
            // Disable header links
 
            event.preventDefault();
 
            // Show and hide the tabs on click
		    $hr = $(this).find(".sbmenu");
            if ($hr.is(":visible"))
            {
            	$hr.slideUp('slow');
            	$(this).removeClass('act');	
            }
            else
            {
            	$('nav.menu[data-rolle=hirzontal] ul li.haveChildren .sbmenu:visible').slideUp();
            	$('nav.menu[data-rolle=hirzontal] ul li.haveChildren.act').removeClass('act');
            	$hr.slideDown('slow');	
            	$(this).addClass('act');	
            }
 
           // if ($(this).attr('class') != 'act'){
               // accordion_body.slideUp('slow');
                //$(this).next().stop(true,true).slideToggle('slow');
               // accordion_head.removeClass('act');
               // $(this).addClass('act');
           // }*/
 
        });
  
  /*SLIDER*/
      $('.company-slider').owlCarousel({
      items: 1,
      loop: true,
      margin: 0,
      nav: true,
      navText: ['',""],
      navContainerClass: "nav-container",
        dots: false, 
        URLhashListener: true,
//      dotsContainer: "class",
      navClass: ["nav-medium nav-left", "nav-medium nav-right"],
    });
//   $('.slider-switcher').owlCarousel({
//      items: 1,
//      loop: false,
//      margin: 0,
//      nav: true,
//      navText: ['',""],
//      navContainerClass: "nav-container",
//        dots: false, 
//     center: true,
//  
////      dotsContainer: "class",
//      navClass: ["nav-medium nav-left", "nav-medium nav-right"],
//        responsive:{
//        0:{
//            nav:false,
//        },
//        960:{
//            nav:true,
//            
//        }
//        }
//
//    });

});






