var gulp = require('gulp'),
    sass = require('gulp-sass'),
    
    uglify = require('gulp-uglify'),                    // минимизация js
    minify = require('gulp-minify-css'),                // минимизация css
    minimg = require('gulp-imagemin'),                  // минимизация img
    
    uncss = require('gulp-uncss'),                      // удаление неис-х классов
    prefix = require('gulp-autoprefixer'),              // префексер
    
    // server
    browsersync = require('browser-sync').create(),     // server
    
    // heplers
    strip = require('gulp-strip-comments'),             // delete comments
    stripcss = require('gulp-strip-css-comments');   // delete css comments
//    rename = require('gulp-rename');


    //email
//    inlineCss = require('gulp-inline-css');           // инлайнер стилей
//    inky = require('inky');                           // шаблонизатор рассылок

        


// PRODUCTION     
gulp.task('prod', ['minimg', 'mincss', 'minjs', 'minhtml']);        

//STYLES
gulp.task('styles', function () {
  return gulp.src('./scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'))

});

gulp.task('minify', function () {
  return gulp.src('./dist/*.css')
    .pipe(minify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist/min'));
    
});

gulp.task('uncss', function () {
    return gulp.src('./css/*.css')
        .pipe(uncss({
            html: ['*.html', 'posts/**/*.html', 'http://example.com']
//            html: ['*.html', 'posts/**/*.html', 'http://example.com']
        }))
        .pipe(gulp.dest('./out'));
});





// EMAILS

//CONVERTE INKY
gulp.task('inky', function() {
  return gulp.src('./templates/**/*.html')
    .pipe(inky())
    .pipe(gulp.dest('./dist'));
});

//INLINE CSS
gulp.task('inline', function () {
  return gulp.src('./dist/*.html')
        .pipe(inlineCss())
        .pipe(gulp.dest('./dist/inlined'));
});


 //REMOVE COMMETNS
gulp.task('strip', function () {
  return gulp.src('./dist/*.html')
    .pipe(strip())
    .pipe(gulp.dest('./dist/nocommetns'));
});
gulp.task('stripcss', function () {
  return gulp.src('./css/*.css')
    .pipe(stripcss())
    .pipe(gulp.dest('./dist/nocommetns'));
});


// PREFIX
gulp.task('prefix', function () {
	return gulp.src('css/style.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('css'));
});



// MINIFY
gulp.task('mincss', function () {
  return gulp.src('./css/*.css')
    .pipe(stripcss())
    .pipe(prefix({
			browsers: ['last 2 versions'],
			cascade: false
		}
  ))
    .pipe(minify())
    .pipe(gulp.dest('./production/css'));
});

gulp.task('minjs', function () {
  return gulp.src('./js/*.js')
    .pipe(strip())
    .pipe(uglify())
    .pipe(gulp.dest('./production/js'));
});

gulp.task('minhtml', function () {
  return gulp.src('./*.html')
    .pipe(strip())
    .pipe(gulp.dest('./production'));
});

gulp.task('minimg', function () {
  return gulp.src('./img/*')
		.pipe(minimg())
		.pipe(gulp.dest('production/img'))
    
});



//WATCH
gulp.task('default',function() {
    
    gulp.watch('./scss/**/*.scss',['styles']);
    gulp.watch('./dist/*.html',['inline']);
    gulp.watch('./templates/**/*.html',['inky']);
});


